#!/usr/bin/env python
###################################################################
#Script Name	:random_exp.py
#Description	:In silico experiment to simulate random HGT.
#Args           :
#Dependecies    :conda create -n randomHGT scipy numpy argparse
#                seaborn matplotlib         
#                conda activate 
#Author       	:Felix Hartkopf
#Email         	:hartkopff@rki.de
###################################################################

import argparse
import random
from collections import Counter
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from scipy.stats import chisquare

if __name__ == "__main__":

    #############
    # Arguments #
    #############   

    # Read command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--repeats', required=True, help="How often should the experiment be repeated.")
    parser.add_argument('-t', '--total', required=True, help="Total number of genes.")
    parser.add_argument('-n', '--genes', required=True, help="Number of genes that are choosen per round.")
    parser.add_argument('-f', '--file', required=True, help="File with counts of real experiment")
    parser.add_argument('--version', action='version',version='0.1')
    args = parser.parse_args()

    total = range(int(args.total))
    repeats = int(args.repeats)
    genes = int(args.genes)
    choosen = []
    countList = [int(line.rstrip('\n')) for line in open(args.file)]

    random.seed(30)

    #######################
    # Simulate gene count #
    #######################

    for round in range(repeats):
        sampling = random.sample(total, k=genes) # sampling without replacement
        #sampling = random.choices(total, k=genes) # sampling with replacement
        choosen = choosen + sampling
    
    counter = Counter(choosen)
    counts = counter.values()

    #########
    # Plots #
    #########

    # Histogram 
    binwidth = 1
    num_bins = range(min(list(counts)+countList), max(list(counts)+countList) + binwidth, binwidth)
    print(num_bins)

    sns.distplot(countList, hist=True, kde=False, 
             bins=num_bins, color = 'purple',
             hist_kws={'edgecolor':'black'})
    sns.distplot(list(counts), hist=True, kde=False, 
             bins=num_bins, color = 'green',
             hist_kws={'edgecolor':'black'})
    plt.ylabel('Number of genes')
    plt.xlabel('Number of recombination events per gene')
    ax2 = plt.twinx()
    plt.ylim(0,0.6)
    plt.ylabel('Frequency')
    sns.distplot(list(counts), hist=False, kde=True, 
             bins=num_bins, color = 'green',
             hist_kws={'edgecolor':'black'}, label='Simulated random recombination events')
    sns.distplot(countList, hist=False, kde=True, 
             bins=num_bins, color = 'purple',
             hist_kws={'edgecolor':'black'}, label='Observed "hybrid" recombination events')


    plt.legend(loc='upper right')
    
    plt.savefig('hist.png')
    plt.savefig('hist.pdf')

    ####################
    # Statistical test #
    ####################

    # Chi^2
    hist_sim, bin_edges_sim = np.histogram(list(counts), bins=num_bins)
    hist_exp, bin_edges_exp = np.histogram(countList, bins=num_bins)
    print(hist_sim)
    print(hist_exp)
    # Compare bins of histograms
    stat_bins = chisquare(f_obs=hist_exp, f_exp=hist_sim)
    print(stat_bins)
    plt.close() 




