Project head
Torsten Semmler <Semmlert(at)rki.de>

Technical head
Lennard Epping <eppingl(at)rki.de>

Active Contributors
Felix Hartkopf <HartkopfF(at)rki.de>
Lennard Epping <eppingl(at)rki.de>

Former Contributors